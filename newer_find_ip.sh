#!/bin/bash

# Function to extract IP addresses from a file
extract_ips() {
    grep -E -o '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)' "$1"
}

# Clear the screen
clear

# Display the script's purpose
echo "$(tput setaf 1)$(tput bold)This script parses through a file and finds all IP addresses. It will then sort and count the duplicates if you choose.$(tput sgr 0)"
read -rsn1 -p "Press any key to continue"

# List files in the current directory
ls -l

# Request file name from the user
echo -e "\nPlease provide the name of the file you wish to parse through"
read -p "Enter a filename: " FN1

# Check if the file exists
if [[ ! -f $FN1 ]]; then
    echo "File $(tput setaf 1)$FN1$(tput sgr 0) does not exist. Please check your filename."
    exit 1
fi

# Prompt for user choice
echo "Select which action you want performed:"
echo "1 - Search for IP addresses in the specified file and list them."
echo "2 - Search for IP addresses, sort them, and remove duplicates."
echo "3 - Exit"
read -p "Enter your selection: " CM

case $CM in
    1)
        echo "Searching and listing IP addresses..."
        extract_ips "$FN1"
        ;;
    2)
        echo "Searching, sorting, and deduplicating IP addresses..."
        extract_ips "$FN1" | sort -u
        ;;
    3)
        echo "Exiting script."
        exit 0
        ;;
    *)
        echo "Invalid selection. Please enter 1, 2, or 3."
        ;;
esac
